from django.test import TestCase, Client

# Create your tests here.

class UnitTest(TestCase):

    def set_up(self):
        self.client = Client()
        self.response = self.client.get('')
        self.page_content = self.response.content.decode('utf8')

    def test_url_exists(self):
        self.assertEqual(self.response.status_code, 200)
    
    def test_template_used(self):
        self.assertTemplateUsed(self.response, 'accordion.html')
