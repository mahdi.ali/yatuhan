from django.urls import path
from . import views

app_name = 'story9'

urlpatterns = [
    path('register/', views.registerView, name = 'register'),
    path('login/', views.loginView, name = 'loginPage'),
    path('logout/', views.logout, name = 'logout'),
]