from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout as django_logout
from .forms import RegistrationForm


def registerView(request):
	form = RegistrationForm()

	if request.method == 'POST':
		form = RegistrationForm(request.POST)
		if form.is_valid():
			form.save()
			user = form.cleaned_data.get('username')
			messages.success(request, 'Account was created for ' + user)

			return redirect('/login/')

	context = {'form':form}

	return render(request, 'register.html', context)

def loginView(request):

	if request.method == 'POST':
		username = request.POST.get('username')
		password = request.POST.get('password')

		user = authenticate(request, username=username, password=password)

		if user is not None:
			login(request, user)
			return redirect('/accordion/')
		else:
			messages.info(request, 'Username or Password is incorrect.')

	context = {}
	return render(request, 'login.html', context)

def logout(request):
	django_logout(request)
	return redirect('/login/')